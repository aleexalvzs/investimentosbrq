package br.com.alexalves.investimentosbrq.consts

object TextsConsts {
    val TextCambio = "Câmbio"
    val TextMoedas = "Moedas"
    val TextVender = "Vender"
    val TextComprar = "Comprar"
}