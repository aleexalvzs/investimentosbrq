package br.com.alexalves.investimentosbrq.model

enum class TypeOperation {
    SALE, PURCHASE
}